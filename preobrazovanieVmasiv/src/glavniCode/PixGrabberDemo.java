package glavniCode;
import java.awt.*;
import java.applet.*;
import java.awt.image.*;

public class PixGrabberDemo extends Applet
{
  Image img;
  
  // ============================================
  
  // init
  
  // ============================================
  public void init()
  {
    img = getImage(getCodeBase(), "img1.jpg");
    
    MediaTracker mt = new MediaTracker(this);
    mt.addImage(img, 0);  
    try
    {
      mt.waitForAll();
    }
    catch(InterruptedException ie)
    {
      return;
    }
    img = dropRed(img);
  }
  
  // ============================================
  
  // dropRed
  
  // ============================================
  Image dropRed(Image img)
  {
    Image imgNew = null;
    int[] pix;
    
    int nImageWidth = img.getWidth(null);
    int nImageHeight = img.getHeight(null);
    
    pix = new int[nImageWidth * nImageHeight];
    
    PixelGrabber pgr = new PixelGrabber(
      img, 0, 0, nImageWidth, nImageHeight,
      pix, 0, nImageWidth);
      
    try
    {
      pgr.grabPixels();  
    }
    catch(InterruptedException ie)
    {
      return null;
    }
    
    for(int i = 0; i < pix.length; i++)
    {
      int nPixel = pix[i];
      
      int nG = 0xff & (nPixel >> 8);
      int nB = 0xff & nPixel;
      
      pix[i] = 0xff000000 | nG << 8 | nB;
    }
    
    imgNew = createImage(
      new MemoryImageSource(
	nImageWidth, nImageHeight, pix,
	0, nImageWidth));
    
    return imgNew;
  }
  
  // ===========================================
  
  // paint
  
  // ===========================================
  public void paint(Graphics g)
  {
    g.drawImage(img, 0, 0, this);
  }  
  
  // ===========================================
  
  // getAppletInfo
  
  // ===========================================
  public String getAppletInfo()
  {
    return "Name: PixGrabber";
  }
}
